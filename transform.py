#!/usr/bin/python
import csv
import json
import petl as etl
import calendar
import urllib
import boto3
import gzip
import os
from decimal import Decimal
from datetime import datetime
from collections import OrderedDict

#Dimension.ORDER_ID,Dimension.ORDER_NAME,Dimension.LINE_ITEM_ID,Dimension.LINE_ITEM_NAME,Dimension.LINE_ITEM_TYPE,Dimension.DATE,DimensionAttribute.ORDER_TRAFFICKER,DimensionAttribute.ORDER_START_DATE_TIME,DimensionAttribute.ORDER_END_DATE_TIME,DimensionAttribute.LINE_ITEM_START_DATE_TIME,DimensionAttribute.LINE_ITEM_END_DATE_TIME,DimensionAttribute.LINE_ITEM_COST_TYPE,DimensionAttribute.LINE_ITEM_COST_PER_UNIT,DimensionAttribute.LINE_ITEM_GOAL_QUANTITY,Column.AD_SERVER_IMPRESSIONS,Column.AD_SERVER_CLICKS



def cleanData(table):
  table = etl.convert(table, 'Column.AD_SERVER_IMPRESSIONS', lambda rec: int(rec.replace(',', '')))
  table = etl.convert(table, 'Column.AD_SERVER_CLICKS', lambda rec: int(rec.replace(',', '')))
  table = etl.convert(table, 'DimensionAttribute.LINE_ITEM_GOAL_QUANTITY', lambda rec: int(rec.replace(',', '')))
  table = etl.convert(table, 'DimensionAttribute.LINE_ITEM_COST_PER_UNIT', lambda rec: rec.replace('$',''))
  table = etl.selectne(table, 'Order', 'Total') #need to strip out the "Total" row that comes at the end of the csv from DFP
  return table

def getOrderTotals(table):
  table1 = table
  aggregation = OrderedDict()
  aggregation['Impressions'] = 'Ad server impressions', sum
  aggregation['Clicks'] = 'Ad server clicks', sum
  table = etl.aggregate(table, 'Order', aggregation)
  table = etl.addfield(table, 'CTR', lambda rec: round(Decimal(rec['Clicks']) / Decimal(rec['Impressions']),4))
  table2 = etl.groupcountdistinctvalues(table1,'Order','Date')
  table = etl.join(table, table2, key='Order')
  table = etl.rename(table, 'value', 'DaysLive')
  table = etl.addfield(table, 'AvgImpressions', lambda rec: round(Decimal(rec['Impressions']) / Decimal(rec['DaysLive']),2))
  table = etl.addfield(table, 'AvgClicks', lambda rec: round(Decimal(rec['Clicks']) / Decimal(rec['DaysLive']),2))
  ## Finding the Start and End Dates
  table3 = etl.convert(table1, 'Date', lambda rec: datetime.strptime(rec,"%m/%d/%y"))
  agg = OrderedDict()
  agg['StartDate'] = 'Date', min
  agg['EndDate'] = 'Date', max
  table3 = etl.convert(table3, 'Date', lambda rec: rec.strftime("%m/%d/%Y")) 
  table3 = etl.aggregate(table3, 'Order', agg)
  table = etl.join(table, table3, key='Order')
  return list(etl.dicts(table)) #return json object

def getDayOfWeekTotals(table):
  aggregation = OrderedDict()
  aggregation['Impressions'] = 'Ad server impressions', sum
  aggregation['Clicks'] = 'Ad server clicks', sum
  table = etl.addfield(table, 'DayOfWeek', lambda rec: datetime.strptime(rec['Date'],"%m/%d/%y").weekday())
  table = etl.sort(table, 'DayOfWeek')
  table = etl.aggregate(table, 'DayOfWeek', aggregation)
  table = etl.convert(table, 'DayOfWeek', lambda rec: calendar.day_name[rec])
  return list(etl.dicts(table)) #return json object

def getDayOfWeekTotalsCallout(table):
  aggregation = OrderedDict()
  aggregation['Impressions'] = 'Ad server impressions', sum
  aggregation['Clicks'] = 'Ad server clicks', sum
  table = etl.addfield(table, 'DayOfWeek', lambda rec: datetime.strptime(rec['Date'],"%m/%d/%y").weekday())
  table = etl.sort(table, 'DayOfWeek')
  table = etl.aggregate(table, 'DayOfWeek', aggregation)
  table = etl.convert(table, 'DayOfWeek', lambda rec: calendar.day_name[rec])
  return list(etl.dicts(table)) #return json object

def getLineItemDate(table):
  aggregation = OrderedDict()
  aggregation['Impressions'] = 'Ad server impressions', sum
  aggregation['Clicks'] = 'Ad server clicks', sum
  table = etl.convert(table, 'Date', lambda rec: datetime.strptime(rec,"%m/%d/%y"))
  table = etl.sort(table, 'Date')
  table = etl.convert(table, 'Date', lambda rec: rec.strftime("%m/%d/%Y")) 
  table = etl.aggregate(table, 'Date', aggregation)
  return list(etl.dicts(table)) #return json object

def getLineItemDateCallout(table):
  aggregation = OrderedDict()
  aggregation['Impressions'] = 'Ad server impressions', sum
  aggregation['Clicks'] = 'Ad server clicks', sum 
  table = etl.convert(table, 'Date', lambda rec: datetime.strptime(rec,"%m/%d/%y"))
  table = etl.aggregate(table, 'Date', aggregation)
  table = etl.convert(table, 'Date', lambda rec: rec.strftime("%m/%d/%Y")) 
  table = etl.addfield(table, 'CTR', lambda rec: round(Decimal(rec['Clicks']) / Decimal(rec['Impressions']),4))
  #Getting the highest values for Impressions, Clicks, and CTR
  ImpressionsTable = etl.sort(table, 'Impressions',reverse=True)
  ImpressionsTable = etl.head(ImpressionsTable, 1)
  ImpressionsTable = etl.addfield(ImpressionsTable, 'Value', 'Impressions')
  ClicksTable = etl.sort(table, 'Clicks',reverse=True)
  ClicksTable = etl.head(ClicksTable, 1)
  ClicksTable = etl.addfield(ClicksTable, 'Value', 'Clicks')
  CtrTable = etl.sort(table, 'CTR',reverse=True)
  CtrTable = etl.head(CtrTable, 1)
  CtrTable = etl.addfield(CtrTable, 'Value', 'CTR')
  table = etl.cat(ImpressionsTable,ClicksTable,CtrTable)
  return list(etl.dicts(table)) #return json object

def getLineItemTotals(table):
  aggregation = OrderedDict()
  aggregation['Impressions'] = 'Ad server impressions', sum
  aggregation['Clicks'] = 'Ad server clicks', sum
  table = etl.aggregate(table, 'Line item', aggregation)
  return list(etl.dicts(table)) #return json object

def getLineItemTotalsTable(table):
  table = etl.convert(table, 'Rate ($)', lambda rec: round(Decimal(rec.strip('$')),2))
  table = etl.addfield(table, 'Budget', lambda rec: round(rec['Rate ($)'] * rec['Goal quantity'] / 1000,2))
  table = etl.addfield(table, 'Value Delivered', lambda rec: round(rec['Rate ($)']*rec['Ad server impressions']/1000),2)
  aggregation = OrderedDict()
  aggregation['Goal Impressions'] = 'Goal quantity', max
  aggregation['Served Impressions'] = 'Ad server impressions', sum
  aggregation['Clicks'] = 'Ad server clicks', sum  
  aggregation['Rate'] = 'Rate ($)', max  
  aggregation['Budget'] = 'Budget', max  
  aggregation['Value Delivered'] = 'Value Delivered', sum  
  table = etl.aggregate(table, 'Line item', aggregation)
  return list(etl.dicts(table)) #return json object


s3 = boto3.client('s3')


def lambda_handler(event, context):
  # This function is inteded to get the exported DFP data file stored in S3. 
  # It will download the file to temp storage and read in the data
  # Next it will transform the data and output it as a json file
  # Finally it will upload this json file to another S3 location where the API can retreive it.  

  print('Loading function')
  # Get the object from the event and show its content type
  bucket = event['Records'][0]['s3']['bucket']['name']
  key = urllib.unquote_plus(event['Records'][0]['s3']['object']['key'].encode('utf8'))
  print 'processsing file: %s/%s' % (bucket,key)
  try:
      tmpgzip = "/tmp/tmp.csv.gz"
      tmpjson = "/tmp/tmp.json"
      s3.download_file(Bucket=bucket, Key=key, Filename=tmpgzip)

      f=gzip.open(tmpgzip,'rb')
      csvdata=f.read()

      table = etl.fromcsv(csvdata,encoding='utf-8')

      # This assumes a file input with data with these headers
      # Order,Line item,Date,Line item start date,Line item end date,Cost type,Rate ($),Goal quantity,Ad server impressions,Ad server clicks
      #data = pd.read_csv(file,encoding='utf-8')
      cleanTable = cleanData(table)
      processed = {}
      processed["body-callout"] = getOrderTotals(cleanTable)
      processed["chart-line-item-perf"] = getLineItemTotals(cleanTable)
      processed["table-line-item-perf"] = getLineItemTotalsTable(cleanTable)
      processed["chart-line-item-day"] = getDayOfWeekTotals(cleanTable)
      processed["chart-line-item-date"] = getLineItemDate(cleanTable)
      processed["callout-line-item-date"] = getLineItemDateCallout(cleanTable)
      
      with open(tmpjson, 'w') as outfile:
        json.dump(processed, outfile)

      # Set the bucket info and upload the data file to s3
      s3location = key.replace('csv.gz', 'json')
      s3bucket = "liteturn-processed"
      try:
          s3.upload_file(Filename=tmpjson, Bucket=s3bucket, Key=s3location)
          # Display results.
          print 'File uploaded to S3 at: %s/%s' % (s3bucket,s3location)
          os.remove(tmpgzip)
          os.remove(tmpjson)
          return(True)
      except Exception as e:
          print(e)
          print('Error putting object {} to bucket {}.'.format(key, bucket))
          raise e

  except Exception as e:
      print(e)
      print('Error getting object {} from bucket {}. Make sure they exist and your bucket is in the same region as this function.'.format(key, bucket))
      raise e

#file1 = "/Users/carl/Downloads/report (5).csv"
#table = etl.fromcsv(file1)
#print(table)#
#

## This assumes a file input with data with these headers
## Order,Line item,Date,Line item start date,Line item end date,Cost type,Rate ($),Goal quantity,Ad server impressions,Ad server clicks
##data = pd.read_csv(file,encoding='utf-8')#
#
#
#

#cleanTable = cleanData(table)
#processed = {}
#processed["body-callout"] = getOrderTotals(cleanTable)
#processed["chart-line-item-perf"] = getLineItemTotals(cleanTable)
#processed["table-line-item-perf"] = getLineItemTotalsTable(cleanTable)
#processed["chart-line-item-day"] = getDayOfWeekTotals(cleanTable)
#processed["chart-line-item-date"] = getLineItemDate(cleanTable)
#processed["callout-line-item-date"] = getLineItemDateCallout(cleanTable)
#print(json.dumps(processed)) 
