# liteturn-processor

## Installation
I would start by creating a virtual environment
https://python-docs.readthedocs.io/en/latest/dev/virtualenvs.html


* `git clone https://github.com/<this-repository>`
* `cd liteturn-processor`
* `pip install petl boto3`

## Running / Development

* `python transform.py`

## Deploying to AWS Lambda
When running the liteturn-processor on an AWS Lambda you'll need to package the dependencies (libs) with the code in a ".zip" file. This is needed because when the code is excuted in the lambda environment it will not execute a pip install. 

I have used this persons script on an AWS t2.medium to get the compiled packages. This may not be needed since I removed the "pandas" dependency. 

https://nervous.io/python/aws/lambda/2016/02/17/scipy-pandas-lambda/

I use the following to package the code in a temporary "deploy" directory.

## Packaging
From project dir:
* `cd venv/lib/python2.7/site-packages/`
* `zip -r9 ~/gl/liteturn-processor/transform.zip *`

Now that you have the libs packaged, you can now just add your app file:

* `zip -r9 ~/gl/liteturn-processor/`
* `zip -g transform.zip transform.py`

You can just keep running the last command when you make changes the the file and upload to lambda
